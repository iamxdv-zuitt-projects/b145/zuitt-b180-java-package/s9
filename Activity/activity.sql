-- ACTIVTY 
/* 
1. Create an 'solution.sql' file inside s04/a1 project and do the following 
using the "music_db" database;
    a. Find all artist that has letter d in its name
    b. Find all songs that has length of less than 230
    c. Joins the 'album' and 'songs' tables. (Only show the album name,song name and song length).
    d. Joins the 'artist' and 'albums' tables. (find all albums that has lette "a" in its name)
    e. Sort the album in Z-A order. (Show only the first 4 records)
    f. Joins the 'album' and 'songs' tables. (Sort albums from Z-A and sort songs from A-Z)
*/

-- Answers:

-- a. 
SELECT * FROM artist WHERE name LIKE "%d%";
-- b.
SELECT * FROM songs WHERE length <= 230
-- c.
SELECT albums.album_title, songs.song_name, songs.length FROM albums
	JOIN songs ON albums.id = songs.album_id;

-- d.
SELECT * FROM artists
	JOIN albums ON artists.id = albums.artist_id WHERE albums.album_title LIKE "%a%";

-- e.
SELECT * FROM albums ORDER BY album_title ASC LIMIT 4;
-- f.
JOIN songs ON albums.id = songs.album_id ORDER BY album_title DESC, song_name ASC;
